// Import the http module using the required directive.
let http = require("http");

//Create a variable port and assign it with the value of 3000.
let port = 3000;

//Create a condition that when the login route is accessed, it will print a message to the user that they are in the login page.

const server = http.createServer((request, response) => {

	if (request.url == "/login"){
		response.writeHead(200, {"Content-Type": "text/plain"})
		response.end("Welcome to the login page.")
		console.log("Success")
	} else {

		response.writeHead(404, {"Content-Type": "text/plain"})
		response.end("I'm sorry the page you are looking for cannot be found")
	}
})

//Create a server using the createServer method that will listen in to the port provided above.

server.listen(port);

//Console log in the terminal a message when the server is successfully running.

console.log(`server now accessible at localhost:${port}.`)


