const express = require("express");
const app = express();
const port = 3000;

app.use(express.json());
app.use(express.urlencoded({extended:true}));

app.get("/home", (req, res) => {
    res.send("Welcome to the Home Page")
})

let users = [
    {
        "username" : "johndoe",
        "password" : "johndoe1234"
    }
]

app.get("/users", (req, res) => {
    res.send(users)
})

app.delete("/delete-user", (req, res) =>{
    let message;
    for(let i = 0; i<users.length; i++){
        if (req.body.username == users[i].username){
            users.splice([i],1);
            message = `User ${req.body.username}'s password has been deleted`;
            break
        } else {
            message = "No users found.";
        }
    } 
    
    res.send(message);
    console.log(users)
})

if(require.main === module){
	app.listen(port, () => console.log(`Server is running at port ${port}`))
}

module.exports = app;
