const Product = require("../models/Product");

//function that creates the product

module.exports.addProduct = (data) =>{

	let newProduct = new Product({
		name : data.product.name,
		description : data.product.description,
		price : data.product.price,
		image : data.product.image
	});

	return newProduct.save().then((product, error) => {
		if(error){
			console.log(error.message)
			return false;
		}else {
			return true;
		};
	})
};

// Retrieve all active Products
module.exports.getAllActive = () => {
	return Product.find({isActive : true}).then(result => {
		return result;
	});
};

// Retrieve all inactive Products
module.exports.getAllInactive = () => {
	return Product.find({isActive : false}).then(result => {
		return result;
	});
};

// Retrieving a specific product
module.exports.getProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then(result => {
		if(result == null){
			return "Product Not Found"
		}else {
			return result
		};
	});
};

// Update a Product
module.exports.updateProduct = (reqParams, data) => {

	// Specify the fields/properties of the document to be updated
	let updatedProduct = {
		image : data.product.image,
		name : data.product.name,
		description : data.product.description,
		price : data.product.price,
		isActive :data.product.isActive
	};

	return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product, error) => {

		// Product not updated
		if(error){
			console.log(error.message)
			return false

		// Product updated successfully
		} else {
			return true;
		}
	})

};

// Archive a product by setting isActive to false

module.exports.archiveProduct = (reqParams) => {
	let archivedProduct = {
		isActive : false
	};

	return Product.findByIdAndUpdate(reqParams.productId, archivedProduct).then((product, error) => {
		if(error){
			console.log(error.message);
			return false;

		// Course updated successfully
		} else {
			return true
		}
	})
};