const Cart = require("../models/Cart");
const Product = require("../models/Product");
const Order = require("../models/Order");
const auth = require("../auth");

// Function to calculate the total amount for a given cart

const calculateTotalAmount = (products) => {
    let totalAmount = 0;
    for (const product of products) {
      totalAmount += product.price * product.quantity;
    }
    return totalAmount;
  };
  

//function that creates the cart

module.exports.orderCart = async (data) => {
    // Check if the user has an existing cart
    const existingCart = await Cart.findOne({ userID: data.userID });

    // Finds the productIds from Product objects
    const productIds = data.cart.products.map((product) => product.productId);

    const productDetails = await Product.find({ _id: { $in: productIds } });

    // Creates the Products array in the order
    const formattedProducts = productDetails.map((product) => {
        const productInfo = data.cart.products.find(
            (p) => p.productId === product._id.toString()
        );

        return {
            productId: product._id.toString(),
            name: product.name,
            description: product.description,
            price: product.price,
            quantity: productInfo.quantity,
            image: product.image
        };
    });

     // Calculate the total amount using the calculateTotalAmount function
    const totalAmount = calculateTotalAmount(formattedProducts);

    if (existingCart) {
        // If an existing cart is found, update the properties
        for (const product of formattedProducts) {
            const existingProduct = existingCart.products.find(
                (p) => p.productId === product.productId
            );

            if (existingProduct) {
                // If the product already exists in the cart, update its quantity
                existingProduct.quantity += product.quantity;
            } else {
                // If the product does not exist in the cart, add it to the products array
                existingCart.products.push({
                    productId: product.productId,
                    name: product.name,
                    description: product.description,
                    price: product.price,
                    quantity: product.quantity,
                    image: product.image
                });
            }
        }
        existingCart.totalAmount += totalAmount;

        return existingCart.save().then((cart, error) => {
            if (error) {
                console.log(error.message);
                return false;
            } else {
                return true;
            }
        });
    } else {
        // If no existing cart is found, create a new cart
        const newCart = new Cart({
            userID: data.userID,
            products: formattedProducts,
            totalAmount: totalAmount,
            specialInstructions: data.cart.specialInstructions,
        });

        return newCart.save().then((cart, error) => {
            if (error) {
                console.log(error.message);
                return false;
            } else {
                return true;
            }
        });
    }
};

module.exports.updateCart = async (data) => {
    // Find the existing cart for the user
    const existingCart = await Cart.findOne({ userID: data.userID });
  
    if (!existingCart) {
      // If the cart does not exist, return an error message
      return { success: false, message: "Cart not found." };
    }
  
    // Update the quantities and shipping instructions for each item in the cart
    data.cart.products.forEach((product) => {
      const existingProduct = existingCart.products.find(
        (p) => p.productId === product.productId
      );
  
      if (existingProduct) {
        existingProduct.quantity += product.quantity;
      }
    });
  
    // Update the shipping instructions in the cart
    existingCart.specialInstructions = data.cart.specialInstructions;
  
    // Recalculate the total amount
    existingCart.totalAmount = calculateTotalAmount(existingCart.products);
  
    // Save the updated cart back to the database
    return existingCart.save().then((cart, error) => {
        if (error) {
            console.log(error.message);
            return false;
        } else {
            return true
        }
        
      });
  };

  module.exports.deleteCart = async (data) => {
    // Find the existing cart for the user
    const existingCart = await Cart.findOne({ userID: data.userID });
  
    if (!existingCart) {
      // If the cart does not exist, return an error message
      return { success: false, message: "Cart not found." };
    }
  
    const productIdToDelete = data.cart.products[0].productId;
  
    // Filter out the product to be deleted from the products array
    existingCart.products = existingCart.products.filter(
      (product) => product.productId !== productIdToDelete
    );
  
    // Recalculate the total amount
    existingCart.totalAmount = calculateTotalAmount(existingCart.products);
  
    // Save the updated cart back to the database
    return existingCart.save()
      .then((cart, error) => {
        if (error) {
          console.error("Error deleting product from cart:", error);
          return false;
        } else {
          return true;
        }
      });
  };

//retrieving user cart
  module.exports.getMycart = async (userData) => {
    return Cart.findOne({ userID: userData.userId }).then((result) => {
      if (result.length === 0) {
        return true;
      } else {
        return result;
      }
    });
  };