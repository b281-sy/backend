const Order = require("../models/Order");
const Product = require("../models/Product");
const auth = require("../auth");

//function that creates the order

module.exports.orderCheckout = async (data) =>{
  // finds the productIds from Product objects
  const productIds = data.orders.products.map((product) => product.productId);

  const productDetails = await Product.find({ _id: { $in: productIds } });
  // Creates the Products array in the order
  const formattedProducts = productDetails.map((product) => {
    const productInfo = data.orders.products.find(
      (p) => p.productId === product._id.toString()
    );

    return {
      productId: product._id.toString(),
      name: product.name,
      description: product.description,
      price: product.price,
      quantity: productInfo.quantity,
    };
  });
  // computes for the total amount
  let totalAmount = 0;

  for (const product of formattedProducts) {
  totalAmount += product.price * product.quantity;
  }

  // Creates the final output
  let newOrder = new Order({
  userID: data.userID,
      products: formattedProducts,
      totalAmount : totalAmount,
      purchasedOn: new Date(),
});

return newOrder.save().then((order, error) => {
  if(error){
    return "Order Processing Failed: " + error.message;

  }else {
    return true
  };
})
};

//function that retrieves the order
module.exports.getAllorders = () => {
	return Order.find({}).then(result => {
		if(result == null){
			return "No new orders";

		}else {
			return result;
		};
	});
}; 

//function that retrieves orders of a user
module.exports.getMyorders = async (userData) => {
    return Order.find({ userID: userData.userId }).then((result) => {
      if (result.length === 0) {
        return "No orders found";
      } else {
        return result;
      }
    });
  };
