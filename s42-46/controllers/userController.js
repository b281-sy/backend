const User = require("../models/User");
const Course = require("../models/Product");
const bcrypt = require("bcrypt");
const auth = require("../auth");

module.exports.checkEmailExists = (reqBody) => {
	return User.find({email : reqBody.email}).then(result => {
		// The find method returns a record if a match is found
		if(result.length > 0){
			return true;

		// No duplicate emails found
		// The user is not registered in the database
		} else {
			return false;
		};
	});
};

module.exports.registerUser = (reqBody) => {

    let newUser = new User({
        firstName : reqBody.firstName,
        lastName : reqBody.lastName,
        email : reqBody.email,
        mobileNo : reqBody.mobileNo,
        password : bcrypt.hashSync(reqBody.password, 10)
    })
    return newUser.save().then((user, error) => {

    // User registration failed
        if(error){
            console.log(error.message)
            return false
        
                // User registration succesful
        } else {
            return true;
        }
     })
};

module.exports.loginUser = (req, res) => {
    return User.findOne({email : req.body.email}).then(result => {

        // User does not exist
        if (result == null) {
            res.status(404).json({error: "User does not exist. Please register to create an account."});
        } 
        // User exists
        else {
            const isPasswordCorrect = bcrypt.compareSync(req.body.password, result.password);

            if (isPasswordCorrect) {
                // Generate an access token
                res.json({ access: auth.createAccessToken(result) });
            } 
            else {
                res.status(401).json({error: "Incorrect password"});
            }
        }
    })
    .catch(error => {
        // Handle any other errors (e.g., database issues)
        console.error(error);
        res.status(500).json({error: "Internal server error"});
    })
};

module.exports.getProfile = (data) => {
	
	return User.findById(data.userId).then(result => {
		
		result.password = "";
		return result;
	});
};

module.exports.setAsAdmin = (reqParams) => {
    let newAdminUser = {
		isAdmin : true
	};

	return User.findByIdAndUpdate(reqParams.id, newAdminUser).then((user, error) => {
		if(error){
			return "Failed to update user" + error.message;

		// Course updated successfully
		} else {
			return "User is now an admin";
		}
	})
};

