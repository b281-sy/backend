const express = require("express");
const mongoose = require("mongoose");
const bodyParser = require('body-parser');

const cors = require("cors");
const userRoutes = require("./routes/userRoutes");
const productRoutes = require("./routes/productRoutes");
const orderRoutes = require("./routes/orderRoutes");
const cartRoutes = require("./routes/cartRoutes");
const app = express();

mongoose.connect("mongodb+srv://jeannytee:gsgT3Rf8vJNhhmsJ@wdc028-course-booking.umqr1ze.mongodb.net/s42-46?retryWrites=true&w=majority",
    {
        useNewUrlParser: true,
        useUnifiedTopology: true
    }
);

let db = mongoose.connection;

db.on('error',console.error.bind(console,"MongoDB connection Error."));
db.once('open',()=>console.log('Now Connected to MongoDB Atlas'));

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));

app.use("/users", userRoutes);
app.use("/products", productRoutes);
app.use("/orders", orderRoutes);
app.use("/cart", cartRoutes);

// Parse JSON bodies for all routes
app.use(bodyParser.json());

app.listen(process.env.PORT || 4000,()=>{
    console.log(`API is not online on port ${process.env.PORT || 4000}`)
})