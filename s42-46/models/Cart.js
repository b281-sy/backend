const mongoose = require("mongoose");

const cartSchema = new mongoose.Schema({
    userID : {
        type:  String,
        require: [true, "UserId is required"]
    },
    products : [{
        productId : {
			type : String,
			required : [true, "productId is required"]
		},
        name : {
            type: String,
        },
        description : {
            type: String,
        },
        price : {
            type: Number
        },
        quantity : {
            type: Number
        },
        image: {
            type: String,
            default: ''
        },
    }],
    totalAmount : {
        type: Number
    },
    addedOn : {
        type: Date,
        default: new Date()
    },
    specialInstructions: {
        type: String,
    }
})

module.exports = mongoose.model("Cart", cartSchema);