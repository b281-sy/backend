const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
    name : {
        type: String,
        require : [true, "Product is required"]
    },
    description :{
        type: String,
        require : [true, "Description is required"]
    },
    price : {
        type: Number,
        require : [true, "Price is required"]
    },
    isActive :{
        type: Boolean,
        default: true
    },
    createdOn : {
        type: Date,
        default: new Date()
    },
    image: {
        type: String,
        default: ''
    }
})

module.exports = mongoose.model("Product", productSchema);