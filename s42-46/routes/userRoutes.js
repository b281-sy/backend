const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController");
const orderController = require("../controllers/orderController");
const auth = require("../auth");

// router.post("/checkEmail", (req, res) => {
// 	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
// });

//Route to check if user exists
router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
});

// Route for user registration
router.post("/register", (req, res) => {
    userController.registerUser(req.body).then(resultFromController => res.send(
        resultFromController
    ));
})

// Route for authentication
router.post("/login", (req,res) =>{
    userController.loginUser(req, res).then(resultFromController => res.send(
        resultFromController));
})

// Route for getting user details

router.get("/details", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization);
	userController.getProfile({userId : userData.id}).then(resultFromController => res.send(resultFromController));
})

// Route for getting all users (Admin only)
router.get("/orders", auth.verify, (req, res) => {

    const userData = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
	}
	
	if(userData.isAdmin == true){
		orderController.getAllorders().then(resultFromController => res.send(resultFromController));
	} else {
		res.send("You do not have access to view all orders");
	}

});

// Route for getting myOrders for the logged in user (nonAdmin)
router.get("/myorders", auth.verify, (req, res) => {

    const userData = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
        userId: auth.decode(req.headers.authorization).id
	}
	
	if(userData.isAdmin == false){
		orderController.getMyorders(userData).then(resultFromController => res.send(resultFromController));
	} else {
		res.send("You are an Admin. Log in to your User account to view orders");
	}

});

// Route for setting user as an admin
router.put("/:id/setasadmin", auth.verify, (req, res) => {

	const data = {
		  isAdmin: auth.decode(req.headers.authorization).isAdmin,
	  }
  
	  if(data.isAdmin == true){
        userController.setAsAdmin(req.params).then(resultFromController => res.send(resultFromController));
	} else {
		  res.send("Only an admin user can change user roles");
	  }
  
  });

module.exports = router;