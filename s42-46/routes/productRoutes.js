const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController");
const auth = require("../auth");

// Route for creating a product for admin only

router.post("/", auth.verify, (req, res) => {
	
	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin == true){
		productController.addProduct(data).then(resultFromController => res.send(resultFromController));
	} else {
		res.send("You do not have access to create product records");
	}

});

// Route for retrieving all the active products
router.get("/", (req, res) => {
	productController.getAllActive().then(resultFromController => res.send(resultFromController));
});

// Route for retrieving all the inactive products
router.get("/archive", (req, res) => {
	productController.getAllInactive().then(resultFromController => res.send(resultFromController));
});

// Route for retrieving a specific product by ID

router.get("/:productId", (req, res) => {
	console.log(req.params.productId);
	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController));
})

// Route for updating a specific product 
router.put("/:productId", auth.verify, (req, res) => {

	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
	}

	if(data.isAdmin == true){
		productController.updateProduct(req.params, data).then(resultFromController => res.send(resultFromController));
	} else {
		res.send("Only and admin user can update products")
	}
	
});

// Route for archiving
router.put("/:productId/archive", auth.verify, (req, res) => {

	const data = {
		  isAdmin: auth.decode(req.headers.authorization).isAdmin,
	  }
  
	  if(data.isAdmin == true){
	  productController.archiveProduct(req.params).then(resultFromController => res.send(resultFromController));
	} else {
		  res.send("Only and admin user can archive products");
	  }
  
  });

module.exports = router;