const express = require("express");
const router = express.Router();
const cartController = require("../controllers/cartController");
const auth = require("../auth");

//Route to create Cart

router.post("/", auth.verify, (req, res) => {
	
	const data = {
		cart: req.body,
		userID: auth.decode(req.headers.authorization).id
	}

	cartController.orderCart(data).then(resultFromController => res.send(resultFromController));
});

//updating Cart Route for auth users 
router.put("/", auth.verify, (req, res) => {
	
	const data = {
		cart: req.body,
        userID: auth.decode(req.headers.authorization).id
	}

	cartController.updateCart(data).then(resultFromController => res.send(resultFromController));
});

//Removing item in Cart Route for auth users 
router.delete("/", auth.verify, (req, res) => {
	
	const data = {
		cart: req.body,
        userID: auth.decode(req.headers.authorization).id
	}

	cartController.deleteCart(data).then(resultFromController => res.send(resultFromController));
});

//Route for getting items in cart:
router.get("/", auth.verify, (req, res) => {

    const userData = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
        userId: auth.decode(req.headers.authorization).id
	}
	
	if(userData.isAdmin == false){
		cartController.getMycart(userData).then(resultFromController => res.send(resultFromController));
	} else {
		res.send("You are an Admin. Log in to your User account to view orders");
	}

});

module.exports = router