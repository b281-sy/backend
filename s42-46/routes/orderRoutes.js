const express = require("express");
const router = express.Router();
const orderController = require("../controllers/orderController");
const auth = require("../auth");

// Route for Order Create

router.post("/checkout", auth.verify, (req, res) => {
	
	const data = {
		orders: req.body,
        userID: auth.decode(req.headers.authorization).id
	}

	orderController.orderCheckout(data).then(resultFromController => res.send(resultFromController));
});

module.exports = router