const User = require("../models/User");
const Course = require("../models/Course");
const bcrypt = require("bcrypt");
const auth = require("../auth");

module.exports.checkEmailExists = (reqBody) => {
	return User.find({email : reqBody.email}).then(result => {
		// The find method returns a record if a match is found
		if(result.length > 0){
			return true;

		// No duplicate emails found
		// The user is not registered in the database
		} else {
			return false;
		};
	});
};

// User registration
module.exports.registerUser = (reqBody) => {
    // Creates a variable named "newUser" and insantiates a new "User" object using the Mongoose model
    let newUser = new User({
        firstName : reqBody.firstName,
        lastName : reqBody.lastName,
        email : reqBody.email,
        mobileNo : reqBody.mobileNo,
        password : bcrypt.hashSync(reqBody.password, 10)
    })

    return newUser.save().then((user, error) => {

        // User registration failed
        if(error){
            return false;

        // User registration succesful
        } else {
        return true;
        }
    })
}


// User authentication
module.exports.loginUser = (reqBody) => {
    // the findOne method returns the first record in the collection that matches the search criteria
    // We use findOne method instead of the find method which returns all records that match the criteria
    return User.findOne({email : reqBody.email}).then(result => {

        // User does not exist
        if(result == null){
            return false

        // User exists
        } else {
            // Creates the variable "isPasswordCorrect" to return the result of comparing the log in from password and the database password
            // The "compareSync" method is used to compare a non encrypted password from the login form to the encrypted password retrieved from the database and returns "True" or "False" value depending on the result.
            const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

            if(isPasswordCorrect){
                // Generate an access token
                // Users the createAccessToken method defined in the auth.js file
                return { access: auth.createAccessToken(result)}
            } else {
                return false;
            }
        }
    })
}

//Activity s38
module.exports.getProfile = (data) => {
	
	return User.findById(data.userId).then(result => {
		
		// Changes the value of the user's password to an empty string when returned to the frontend
		result.password = "";
		return result;
	});
};

// Enroll user to a class
// Async await will be used in enrolling the user because we will be updating 2 separate documents when enrolling a user
module.exports.enroll = async (data) => {

	// Using the "await" keyword will allow the enroll method to complete updating the user before returing a response back
	let isUserUpdated = await User.findById(data.userId).then(user => {

		// Adds the courseId in the users enrollment array
		user.enrollments.push({courseId : data.courseId});

		// Saves the updated user information in the database
		return user.save().then((user, error) => {
			if(error){
				return false
			}else {
				return true;
			}
		});
	});

	let isCourseUpdated = await Course.findById(data.courseId).then(course => {
		// Adds the userId in the course's enrollees array
		course.enrollees.push({userId : data.userId});

		return course.save().then((course, error) => {
			if(error){
				return false;
			} else {
				return true;
			};
		});

	});

	if(isUserUpdated && isCourseUpdated){
		return true;
	// User enrollment failure
	} else {
		return false
	};
}
