// Array Traversal

let grades = [98.5, 94.3, 89.2, 90.1];
let computerBrands = ['Acer', 'Asus', 'Lenovo', 'Neo', 'Redfox', 'Gateway', 'Toshiba', 'Fujitsu'];

let mixedArr = [12, 'Asus', null, undefined]; // This is not recommended

let tasks = [
	'drink html',
	'eat javascript',
	'inhale css',
	'bake sass'
];

// Creating an array with values from variables
let city1 = "Tokyo";
let city2 = "Manila";
let city3 = "Mumbai";

let cities = [city1, city2, city3];

console.log(tasks);
console.log(cities);

// Array length property
console.log("Array length");
console.log(tasks.length);
console.log(cities.length);

let fullName = "Aizaac Estiva";
console.log(fullName.length);

tasks.length = tasks.length - 1;
console.log(tasks.length);
console.log(tasks);

cities.length--;
console.log(cities);

fullName.length = fullName.length - 1;
console.log(fullName.length);

// Adding a number to lengthen the size of the array
let theBeatles = ["John", "Paul", "Ringo", "George"];
theBeatles.length++;
console.log(theBeatles);

// Accessing elements of an array;

console.log(grades[0]);
console.log(computerBrands[3]);
console.log(grades[20]);

let lakersLegends = ["Kobe", "Shaq", "LeBron", "Magic", "Kareem"];
// Access the second item in the array:
console.log(lakersLegends[1]);
// Access the fourth item in the array:
console.log(lakersLegends[3]);
// index = numberItem - 1;

let currentLaker = lakersLegends[2];
console.log("Accessing arrays using variables")
console.log(currentLaker);

// Reassigning values in array
console.log("Array before reassignment:");
console.log(lakersLegends);
lakersLegends[2] = "Pau Gasol";
console.log('Array after reassignment:');
console.log(lakersLegends);

// Accessing the last element of an array
let bullsLegends = ["Jordan", "Pippen", "Rodman", "Rose", "Kukoc"];

let lastElementIndex = bullsLegends.length - 1;

console.log(bullsLegends[lastElementIndex]);

// Directly access the expression
console.log(bullsLegends[bullsLegends.length - 1]);
