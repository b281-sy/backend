// console.log("Hello World")

// //Objective 1
// let string = 'supercalifragilisticexpialidocious';
// console.log(string);
// let filteredString = '';

// //Add code here

// for (let i = 0; i < string.length; i++){
//     if(
//         string[i].toLowerCase() == "a" ||
//         string[i].toLowerCase() == "e" ||
//         string[i].toLowerCase() == "i" ||
//         string[i].toLowerCase() == "o" ||
//         string[i].toLowerCase() == "u"
//     ){
//         continue;
//     } else {filteredString+=string[i]}
//     }

// console.log(filteredString);


// //Do not modify
// //For exporting to test.js
// //Note: Do not change any variable and function names. All variables and functions to be checked are listed in the exports.
// try{
//     module.exports = {

//         filteredString: typeof filteredString !== 'undefined' ? filteredString : null

//     }
// } catch(err){

// }

function purchase(age, price) {
    if (age < 13) {
        return undefined;
      } else if (age >= 13 && age <= 21 || age >= 60) {
        const discountedPrice = Math.round(price * 0.8);
        return discountedPrice.toString();
      } else {
        const roundedPrice = Math.round(price);
        return roundedPrice.toString();
      }
    // Return undefined for people aged below 13.
    // Return the discounted price (rounded off) for students aged 13 to 21 and senior citizens. (20% discount)
    // Return the rounded off price for people aged 22 to 59.
    // The returned value should be a string.

    
}

console.log(purchase(18, 83))