// CRUD Operations

// Create
// Inserting one collection
db.users.insertOne({
    "firstName" : "Jane",
    "lastName" : "Doe",
    "age" : 21,
    "contact" : {
        "phone" : "85479123",
        "email" : "janedoe@gmail.com"
    },
    "course" : ["CSS", "JavaScript", "Python"],
    "department" : "none"
});

// Inserting many collection
db.users.insertMany([
    {
        "firstName" : "Stephen",
        "lastName" : "Hawking",
        "age" : 76,
        "contact" : {
            "phone" : "85479123",
            "email" : "stephenh@gmail.com"
        },
        "course" : ["React", "PHP", "Python"],
        "department" : "none"
    },
    {
        "firstName" : "Neil",
        "lastName" : "Armstrong",
        "age" : 82,
        "contact" : {
            "phone" : "85479123",
            "email" : "neilarms@gmail.com"
        },
        "course" : ["React", "PHP", "Python"],
        "department" : "none"
    },
]);

// Finding documents (Read)
// Find operator

// Retrieving all documents
db.users.find();

// Retrieving Single documents
db.users.find({"firstName": "Stephen"});

// Retrieving documents with single parameters
db.users.find({"lastName": "Armstrong", "age":32});

//Updating Documents (update)

//Update a single document to update

//creating a document to update
db.users.insertOne({
    "firstName" : "Test",
    "lastName" : "Test",
    "age" : 0,
    "contact" : {
        "phone" : "00000000",
        "email" : "testemail@gmail.com"
    },
    "course" : [],
    "department" : "none"
});

db.users.updateOne(
    {"firstName": "Test"},
    {
        $set : {
            "firstName": "Bill",
            "lastName": "Gates",
            "age": "65",
            "contact": {
                "phone": "87654321",
                "email": "billgates@gmail.com"
            },
            "course": ["php", "laravel", "HTML"],
            "department": "operations",
            "status": "active"
        }
    }
);

db.users.updateMany(
    {"department" : "none"},
    {
        $set : {"department": "HR"}
    }
);

//ReplaceOne

db.users.replaceOne(
    { "firstName":"Bill"},
    {
        "firstName":"Bill",
        "lastName" : "Gates",
        "age": 65,
        "contact":{
            "phone":"87654321",
            "email":"bill@mail.com"

                },
            "course" : ["PHP","Laravel","HTML"],
            "department":"Operations"
    }
);

//Deleting documents 

//creating a document to delete
db.users.insertOne({
    "firstName" : "test"
});

// Deleting a single document
db.users.deleteOne({
    "firstName" : "test"
});

// Query an embedded document
db.users.find({
    contact: {
        "phone": "87654321",
        "email":"bill@mail.com"
    }
});
