// console.log("Hello World");

//Strictly Follow the property names and spelling given in the google slide instructions.
//Note: Do not change any variable and function names. 
//All variables and functions to be checked are listed in the exports.

// Create an object called trainer using object literals

// Initialize/add the given object properties and methods

// Properties

let trainer = {
	name: "Ash Ketchum",
	age: 10,
	Pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
	friends: {
		hoenn: ["May", "Max"],
		kanto: ["Brock", "Misty"],
	},
	talk: function(){
		console.log("Pikachu! I choose you!");
	} 
}

console.log(trainer);

// Methods

// Check if all properties and methods were properly added

// Access object properties using dot notation

// Access object properties using square bracket notation

// Access the trainer "talk" method

console.log("Result of dot notation:");
console.log(trainer.name);

console.log("Result of square bracket notation:");
console.log(trainer['Pokemon']);

/*7. Invoke/call the trainer talk object method.*/

console.log("Result of talk method:");
trainer.talk();



// Create a constructor function called Pokemon for creating a pokemon

function Pokemon(name, level) {
	this.name = name;
	this.level = level;
	this.health = 2 * level;
	this.attack = level;
	this.tackle = function(target){
		console.log(this.name + " tackled " + target.name);
		console.log(target.name + " health is now reduced to " + Number(target.health - this.attack));
		target.health = target.health - this.attack;
		if (target.health <= 0){
			this.faint(target);
		}
	}
	this.faint = function(target){
				console.log(target.name + " fainted. ");
		};
};

// Create/instantiate a new pokemon


// Create/instantiate a new pokemon


// Create/instantiate a new pokemon


// Invoke the tackle method and target a different object


// Invoke the tackle method and target a different object

let pikachu = new Pokemon("Pikachu", 12);
console.log(pikachu);

let geodue = new Pokemon("Geodue", 8);
console.log(geodue);

let mewtwo = new Pokemon("Mewtwo", 100);
console.log(mewtwo);

geodue.tackle(pikachu);

console.log(pikachu);

mewtwo.tackle(geodue);

console.log(geodue);



//Do not modify
//For exporting to test.js
//Note: Do not change any variable and function names. All variables and functions to be checked are listed in the exports.
try{
    module.exports = {

        trainer: typeof trainer !== 'undefined' ? trainer : null,
        Pokemon: typeof Pokemon !== 'undefined' ? Pokemon : null

    }
} catch(err){

}