//Important Note: Do not change the variable names. 
//All required classes, variables and function names are listed in the exports.

// Exponent Operator

/*Create a variable getCube and use the exponent operator to compute for the cube of a number. (A cube is any number raised to 3)*/

const getCube = (num) => {
    let numCube = Math.pow(num, 3);
    console.log(`The cube of ${num} is ${numCube}`);
};

getCube(2);

// Template Literals


// Array Destructuring

/* Create a variable address with a value of an array containing details of an address.*/
const address = ["258", "Washington Ave NW", "California", "90011"];

/*Destructure the array and print out a message with the full address using Template Literals.*/

const [houseNumber, street, state, zipCode] = address;

console.log(`I live at ${houseNumber} ${street}, ${state} ${zipCode}`);


// Object Destructuring
// Create a variable animal with a value of an object data type with different animal details as it’s properties.
const animal = {
	name: "Lolong",
	species: "saltwater crocodile",
	weight: "1075 kgs",
	measurement: "20 ft 3 in"
}
// Destructure the object and print out a message with the details of the animal using Template Literals.*/
console.log(`${animal.name} is a ${animal.species}. He weighed at ${animal.weight} kgs with a measurement of ${animal.measurement}`);

// Arrow Functions
// Create an array of numbers.

let numbers = [1, 2, 3, 4, 5];
/* Loop through the array using forEach, an arrow function and using the implicit return statement to print out the numbers.*/
numbers.forEach((number) => {
    console.log(`${number}`);
});

/*Create a variable reduceNumber and using the reduce array method and an arrow function console log the sum of all the numbers in the array.*/

const initialValue = 0;
const reduceNumber = numbers.reduce(
(x, y) => x + y, initialValue);

console.log(reduceNumber);

// Javascript Classes
/* Create a class of a Dog and a constructor that will accept a name, age and breed as it’s properties.

13. Create/instantiate a new object from the class Dog and console log the object.*/
class Dog {
    constructor(name, age, breed){
        this.name = name;
        this.age = age;
        this.breed = breed;
    }
    };

//    Create/instantiate a new object from the class Dog and console log the object.

const myNewdog = new Dog("Frankie", 5, "Miniature Dachschund");
console.log(myNewdog);

//Do not modify
//For exporting to test.js
//Note: Do not change any variable and function names. All variables and functions to be checked are listed in the exports.
try{
    module.exports = {

        getCube: typeof getCube !== 'undefined' ? getCube : null,
        houseNumber: typeof houseNumber !== 'undefined' ? houseNumber : null,
        street: typeof street !== 'undefined' ? street : null,
        state: typeof state !== 'undefined' ? state : null,
        zipCode: typeof zipCode !== 'undefined' ? zipCode : null,
        name: typeof name !== 'undefined' ? name : null,
        species: typeof species !== 'undefined' ? species : null,
        weight: typeof weight !== 'undefined' ? weight : null,
        measurement: typeof measurement !== 'undefined' ? measurement : null,
        reduceNumber: typeof reduceNumber !== 'undefined' ? reduceNumber : null,
        Dog: typeof Dog !== 'undefined' ? Dog : null

    }
} catch(err){

}




















