// [SECTION] Selection Control stuctures
    /* 
        - sorts out whether the statement/s are to be exceuted based
        on the condition whether it is true or false.
        - two-way (true or false)
        - multi-way selection

    */

    // If ... else statement

    /*
        Syntax:
            if(condition) {
                //statement
            } else {
                //statement
            }
    
    */

    // If statement - executes a statment if a specific condition is true.

    let numA = -1;

    if(numA < 0) {
        console.log("The number is less than 0");
    } 

    console.log(numA < 0);

    let city = "New York";

    if( city === "New York" ) {
        console.log("Welcome to New York City!");
    } 

    let numB = 1;

    if(numA > 0){
        console.log("Hello");
    } else if (numB > 0) {
        console.log("Word");
    }

    city = "Tokyo"

    if(city === "New York") {
        consolelog("Welcome to New York Cit!");
    } else if (city === "Tokyo") {
        console.log("Welcome to Tokyo!");
    }



    /* minutes

        Mini Activity
        - Create a function that will eceive any value of height as an argument whe  you invoke it
        - then create a condition statements:
            - If the hight is less than or equal to 150, print "Did not pass the min heigh requirement"
            -  but if the height is greater than 150, print "Passed the min height requirement." in the console
    */

function heightCalculator(height) {
    if (height <= 150) {
        console.log("Did not pass the min heigh requirement");
    } else if (height >= 150) {
        console.log("Passed the min height requirement.")
    }
}

heightCalculator(130);

let message = "No Message";
console.log(message);

function determineTyphoonIntensity(windSpeed) {
    if(windSpeed < 30) {
        return 'Not a Typhoon'
    } else if(windSpeed <=60) {
        return 'Tropical depression detected'
    } else if(windSpeed >= 62 && windSpeed <=88) {
        return 'Tropical storm detected' 
    } else if(windSpeed >=89 && windSpeed <=188) {
        return 'Severe tropical storm detected'
    } else 'typhoon detected'
}
        
message = determineTyphoonIntensity(90);
console.log(message);
        
// Truthy and Falsy

/*
	In JS a truthy value is a value that is considered true when encountered in a boolean context.

	Falsy value:
		1. false
		2. 0
		3. -0
		4. ""
		5. null
		6. undefined
		7. NaN
*/	

// Truthy Examples:

let word = "true"
if(word){
	console.log("Truthy");
};

if(true) {
	console.log("Truthy");
};

if(1) {
	console.log("Truthy");
};


// Falsy Examples:
if(false) {
	console.log("Falsy");
};

if(0) {
	console.log("Falsy");
};

if(undefined) {
	console.log("Falsy");
};

if(null) {
	console.log("Falsy");
};

if(-0) {
	console.log("Falsy");
};

if(NaN) {
	console.log("Falsy");
  };       
        

// condition (Ternary) Operator - for short codes

/*
    Ternary operator takes in three operands
        1. condition
        2. expression to execute if the condition is true/truthy.
        3. expression to execture if the condition is false/falsy.

    Syntax:
        (Condition) ? ifTrue_expression : isFalse_expression.
*/

// Example of single statement execution

let ternaryResult = (1 < 18) ? "Condition is True" : "Condition is False";

console.log("result of the ternary operator: " + ternaryResult);

// Mutiple Statement exectution

let name;
function isofLegalAge() {
    name = "John";
    return "You are of the legal age limit"
}

function isUnderAge() {
    name = "Jane"
    return "You are under the legal age limit"
}

// let yourAge = parseInt(prompt("What is your age?"));
// console.log(yourAge)

// let legalAge = (yourAge > 18) ? isofLegalAge() : isUnderAge();

// console.log("result of ternary operator in function: " + legalAge + " " + name);

// [Section] Switch Statement

/*
    Can be used as an alternative to if ... else statements where the data to be used in the condition is of an expected input.

    Syntax:
        switch (expression/condition) {
            case <value>:
                statement;
                break;
            default:
                statement;
                break;
        }
*/

let day = prompt("What day of the week is it today?").toLowerCase();

console.log(day);

switch (day) {
    case 'monday': 
        console.log("The color of the day is Red");
        break;
    case 'tuesday': 
        console.log("The color of the day is Orange");
        break;
    case 'wednesday': 
        console.log("The color of the day is Yellow");
        break;
    case 'thursday': 
        console.log("The color of the day is Green");
        break;
    case 'friday': 
        console.log("The color of the day is Blue");
        break;
    case 'saturday': 
        console.log("The color of the day is Indigo");
        break;
    case 'sunday': 
        console.log("The color of the day is Violet");
        break;
    default:
        console.log("Please input a valid day")
        break
    
}
        
// break; - is for ending the loop.
// Switch - is not a very popular statement in coding.


// [Section] Try-catch-finally statement

/*
 - try catch is commonly used for error handling.
 - will still function even if the statement is not complete/ without the finally code block.
*/

function showIntensityAlert(windSpeed) {
    try{
        alert(determineTyphoonIntensity(windSpeed))
    }
    catch (error) {
        console.log(typeof error)
        console.log(error)
        console.warn(error.message)
    }
    finally {
        alert("Intensity updates will show new alert!")
    }
}

showIntensityAlert();

        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
    