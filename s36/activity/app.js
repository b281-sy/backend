//Setup our dependencies
const express = require("express");
const mongoose = require("mongoose");
const taskRoute = require("./routes/taskRoute"); //creating a route that points to a route folder (make sure that the routes folder is created//

// Server setup
const app = express();
const port = 2001;
app.use(express.json());
app.use(express.urlencoded({extended:true}));

//connect MongoDB Connection
mongoose.connect("mongodb+srv://jeannytee:gsgT3Rf8vJNhhmsJ@wdc028-course-booking.umqr1ze.mongodb.net/?retryWrites=true&w=majority",
		{
			useNewUrlParser: true,
			useUnifiedTopology: true
		}
);

mongoose.connection.once("open", () => console.log("Now Connected to the Database!"));

// Add the task route
app.use("/tasks", taskRoute); // By writing this, all the task routes will start with tasks

// server listening

app.listen(port, () => console.log(`Now listening to port ${port}`));
