const express = require("express");
const router = express.Router();

const taskController = require("../controllers/taskController");

// Get all tasks

router.get("/", (req, res) => {

	// it invokes the gettAllTasls function from the taskController.js
	taskController.getAllTasks().then(resultFromController => res.send(
		resultFromController));
});

// [ SECTION ] Create a new task
router.post("/", (req,res) => {
	taskController.createTask(req.body).then(resultFromController => res.send(
		resultFromController));
})

// [ SECTION ] Delete a task
router.delete("/:id", (req, res) => {
	taskController.deleteTask(req.params.id).then(resultFromController => res.send(
		resultFromController));
})

router.put("/:id", (req, res) => {

	taskController.updateTask(req.params.id, req.body).then(resultFromController => res.send(
		resultFromController));
})

router.get("/:id", (req, res) => {

	// it invokes the gettAllTasls function from the taskController.js
	taskController.getAllTasks(req.params.id, req.body).then(resultFromController => res.send(
		resultFromController));
});

router.put("/:id/complete", (req, res) => {

	taskController.updateTask(req.params.id, req.body).then(resultFromController => res.send(
		resultFromController));
})

module.exports = router;
