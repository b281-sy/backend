const express = require("express");
const mongoose = require("mongoose");

const app = express();
const port = 3001;

// MongoDB connection
// Connecting to MongoDB Atlas
mongoose.connect("mongodb+srv://jeannytee:gsgT3Rf8vJNhhmsJ@wdc028-course-booking.umqr1ze.mongodb.net/b281_to-do?retryWrites=true&w=majority",
    {
        useNewUrlParser : true,
        useUnifiedTopology : true
    }
);

// Set notification for connection success or failure
let db = mongoose.connection;

// If a connection error occurred, output in the console
db.on("error", console.error.bind(console, "connection error"));

// If a connection is succesful, output in the console
db.once("open", () => console.log("We're connected to the cloud database"));

// Mongoose Schemas
const taskSchema = new mongoose.Schema({
    name : String,
    status : {
        type : String,
        default : "pending"
    }
})


//Models [Task models]
const Task = mongoose.model("Task", taskSchema);
// Creation of todo list routes

// allows the app to read json data
app.use(express.json());
// allows the app to read data from forms
app.use(express.urlencoded({extended:true}));

// Creating a new Task
app.post("/tasks", (req,res) => {
    //if a document was found and document's name matches the information from the client
    Task.findOne({name : req.body.name}).then((result, err) => {
        if(result != null && result.name == req.body.name){
            // Return a message to the client/Postman
            return res.send("Duplicate task found");
        }
        // If no document was found
        else{
            // Create a new tasl and saveg it to the database
            let newTask = new Task({
                name : req.body.name
            });

            newTask.save().then((savedTask, saveErr) => {
                // If there are errors in saving
                if(saveErr){
                    return console.log(saveErr);
                }
                // No error found while creating the document
                else{
                    return res.status(201).send("New task created");
                }
            })
        }
    })
})

// Get All the tasks
app.get("/tasks", (req, res) => {
    Task.find({}).then((result, err) => {
        // If an error occured
        if(err){
            // Will print any errors found in the console
            return console.log(err);
        }
        // If no errors are found
        else{
            return res.status(200).json({
                data : result
            })
        }
    })
})

/*Create a User schema.*/

const userSchema = new mongoose.Schema({
	username : String,
	password : String
})

/*Create a User model.*/
const User = mongoose.model("User", userSchema);

/*Create a POST route that will access the /signup route that will create a user.*/

app.post("/signup", (req,res) => {
    //if a document was found and document's name matches the information from the client
    User.findOne({username : req.body.username}).then((result, err) => {
        if(result != null && result.username == req.body.username){
            // Return a message to the client/Postman
            return res.send("Duplicate user found");
        }
        // If no document was found
        else{
            // Create a new tasl and saveg it to the database
            let newUser = new User({
                username: req.body.username,
			    password: req.body.password
            });

            newUser.save().then((savedUser, saveErr) => {
                // If there are errors in saving
                if(saveErr){
                    return console.log(saveErr);
                }
                // No error found while creating the document
                else{
                    return res.status(201).send("New User Registered!");
                }
            })
        }
    })
})



// Listen to the port
app.listen(port, () => console.log(`Server running at port ${port}`));
